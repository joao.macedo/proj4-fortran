# Fortran-Proj4

The Fortran-Proj4 library provides fortran binding functions for the Proj4 native library (C).
<p>
PROJ is a generic coordinate transformation software, that transforms geospatial coordinates from one coordinate reference system (CRS) to another. This includes cartographic projections as well as geodetic transformations.
<\p>
<p>
Detailed information about the native Proj4 library can be found at:
https://proj4.org/index.html
<\p>

**First of all, don't forget to install the native Proj4 headers (libproj-dev)...**


* DOWNLOAD, BUILD AND INSTALL (Fortran-Proj4):

```
~$ git clone https://gitlab.com/joao.macedo/proj4-fortran.git
~$ mkdir proj4-fortran./build
~$ cd proj4-fortran/build
```
run CMake inside the build directory:
```
~$ cmake ..
```
alternatively you can specify native Proj4 path, if not installed in system default paths, and/or specify the install destination path.
```
~$ cmake -DCMAKE_INCLUDE_PATH=/native/proj4/path/include/\
         -DCMAKE_LIBRARY_PATH=/native/proj4/path/lib/\
         -DCMAKE_INSTALL_PREFIX=/fproj4/install/path ..                           
```

```
~$ make
~$ make install
```

**Run the _fproj4_example_ program inside the build directory to make sure it has been successfully installed and take a look inside example/fproj4_example.f90 to understand how to use Fortran-Proj4.**

* COMPILE YOUR CODE:

Compile your fortran program using the <code>-lfproj4 -lproj</code> flags
to link the Proj4 library and <code>-c -I</code> to specify
the include path containing the _fproj4.mod_ anf _proj.h_ files.
```
~$ <FC> -c -I/fproj4/install/path/include -o your_fortran_program.o your_fortran_program.f90
~$ <FC> your_fortran_program.o -lfproj4 -lproj  -o your_fortran_program
```

(Replace \<FC\> by your fortran compiler)

Special thanks to [Magnus Hangdorn](mailto:magnus.hagdorn@marsupium.org) 
that has given an inspiring contribute for this work!


