
program fproj4_example

  use fproj4
  implicit none

  integer                :: status
  real(kind=kind(1.0d0)) :: lam0, phi0, x0, y0,&
                            lam1(2), phi1(2), x1(2), y1(2),&
                            lam2(2,2), phi2(2,2), x2(2,2), y2(2,2)
  type(fproj4_prj)       :: proj, srcdefn, dstdefn
  character(len=256)     :: proj_str

  ! Testing FPJ4_UNKNOWN_PROJ_ID
  proj_str = '+proj=testing_error '//&
             '+ellps=WGS84 '//&
             '+lat_1=52.8333320617676 '//&
             '+lat_2=68.1666641235352 '//&
             '+lon_0=33.5'//&
             '+lat_0=60.5 '//&
             '+x_0=1903970.98145531 '//&
             '+y_0=898179.31322811'

  status=fproj4_init(proj, proj_str)
  if (status .eq. FPJ4_UNKNOWN_PROJ_ID) then
     print*, fproj4_strerrno(status)
  end if

  proj_str = '+proj=aea '//&
             '+ellps=WGS84 '//&
             '+lat_1=52.8333320617676 '//&
             '+lat_2=68.1666641235352 '//&
             '+lon_0=33.5'//&
             '+lat_0=60.5 '//&
             '+x_0=1903970.98145531 '//&
             '+y_0=898179.31322811'

  status = fproj4_init(proj, proj_str)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if

  lam0 = 7.0
  phi0 = 49.0
  x0 = 0
  y0 = 0

  ! Testing point 
  status = fproj4_fwd_pt(proj, lam0, phi0, x0, y0)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if  

  print*, lam0, phi0, x0, y0

  lam0 = 0
  phi0 = 0

  status = fproj4_inv(proj, x0, y0, lam0, phi0)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if    

  print*, x0, y0, lam0, phi0

  lam1 = [59.92093, 60.92093]
  phi1 = [71.9509, 72.9509]
  x1 = [0, 0]
  y1 = [0, 0] 

  status = fproj4_fwd(proj, lam1, phi1, x1, y1)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if  
  print*, lam1, phi1, x1, y1
  lam1 = [0, 0]
  phi1 = [0, 0]

  status = fproj4_free(proj)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if    

  status = fproj4_init(srcdefn, proj_str)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if

  status = fproj4_init(dstdefn, LATLONG_PRJ_STR)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if

  status = fproj4_transfer(srcdefn, dstdefn ,x1, y1, lam1, phi1)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if    

  print*, x1, y1, lam1*RAD2DEG, phi1*RAD2DEG

  lam2 = reshape([59.92093, 60.92093, 59.92093, 60.92093] , shape(lam2))
  phi2 = reshape([71.9509, 72.9509, 71.9509, 72.9509], shape(phi2))
  x2 = lam2 * DEG2RAD 
  y2 = phi2 * DEG2RAD 

  status = fproj4_transform(dstdefn, srcdefn, x2, y2)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if    
  print*, lam2, phi2, x2, y2

  status = fproj4_free(srcdefn)
  if (status.ne.FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if    

  status = fproj4_free(dstdefn)
  if (status .ne. FPJ4_NOERR) then
     print*, fproj4_strerrno(status)
     stop
  end if    

end program fproj4_example



!> NOTE: The following interfaces can handle point data,
!!       1D array and 2D array.


!>              INTERFACE: fproj4_transform
!!
!> @param[in] srcdefn Source (input) coordinate system.
!!
!> @param[in] dstdefn Destination (output) coordinate system.     
!!
!> @param[inout] x, y, z The array/element of X, Y and Z coordinate values
!!                        passed as input, and modified in place for output.
!!                        The Z may be optional.
!!
!> @param[in] point_offset The step size from value to value (measured in doubles)
!!                          within the x/y/z arrays - normally 1 for a
!!                          packed array. May be used to operate on xyz interleaved
!!                          point arrays. It is an optional argument.
!!
!> @return stat The return is zero on success, or a PROJ.4 error code.



!>              INTERFACE: fproj4_transfer
!!
!> @param[in] srcdefn Source (input) coordinate system.
!!
!> @param[in] dstdefn Destination (output) coordinate system.     
!!
!> @param[in] x, y The array/element of X and Y coordinate values
!!                 passed as input, in the source coordinate system.
!!
!> @param[out] x_t, y_t The array/element of X and Y coordinate values
!!                      where the output will be stored, in the
!!                      destination coordinate system.
!!
!> @return stat The return is zero on success, or a PROJ.4 error code.


!>              INTERFACE: fproj4_fwd
!!
!> @param[in] prjdefn Destination (output)  coordinate system.
!!
!> @param[in] x, y The array/element of X and Y coordinate values
!!                 passed as input, in the Geographic (long,lat)
!!                 coordinate system.
!!
!> @param[out] x_t, y_t The array/element of X and Y coordinate values
!!                      where the output will be stored, in the
!!                      destination coordinate system.
!!
!> @return stat The return is zero on success, or a PROJ.4 error code.


!>              INTERFACE: fproj4_inv
!!
!> @param[in] prjdefn Source (input)   coordinate system.
!!
!> @param[in] x, y The array/element of X and Y coordinate values
!!                 passed as input, in the source coordinate system.
!!
!> @param[out] x_t, y_t The array/element of X and Y coordinate values
!!                      where the output will be stored, in the Geographic
!!                      (long,lat) coordinate system.
!!
!> @return stat The return is zero on success, or a PROJ.4 error code.



!>              ERROR CODES LIST:
!!
!> FPJ4_NOERR                                =   0
!! errors not found
!!
!> FPJ4_MISS_INIT_LIST_ARGS                  =  -1
!! no arguments in initialization list
!!
!> FPJ4_MISS_INIT_FILE_OPTS                  =  -2
!! no options found in 'init' file
!!
!> FPJ4_MISS_COLON_INIT_STR                  =  -3
!! no colon in init= string
!!
!> FPJ4_MISS_PROJ_NAME                       =  -4
!! projection not named
!!
!> FPJ4_UNKNOWN_PROJ_ID                      =  -5
!! unknown projection id
!!
!> FPJ4_INVALID_EFFECT_ECCENTRICITY          =  -6
!! effective eccentricity = 1
!!
!> FPJ4_UNKNOWN_UNIT_CONV_ID                 =  -7
!! unknown unit conversion id
!!
!> FPJ4_INVALID_BOOL_PARAM                   =  -8
!! invalid boolean param argument
!!
!> FPJ4_UNKNOWN_ELLIPTICAL_PARAM             =  -9
!! unknown elliptical parameter name
!!
!> FPJ4_INVALID_FLATTENING                   = -10
!! reciprocal flattening (1/f) = 0
!!
!> FPJ4_INVALID_RADIUS_REF_LAT               = -11
!! |radius reference latitude| > 90
!!
!> FPJ4_INVALID_ECCENTRICITY                 = -12
!! squared eccentricity < 0
!!
!> FPJ4_MISS_EARTH_RADIUS                    = -13
!! major axis or radius = 0 or not given
!!
!> FPJ4_LAT_LON_OUT_OF_BOUNDS                = -14
!! latitude or longitude exceeded limits
!!
!> FPJ4_INVALID_X_Y                          = -15
!! invalid x or y
!!
!> FPJ4_DMS_FORMAT_ERR                       = -16
!! improperly formed DMS value
!!
!> FPJ4_NON_CONVERGENT_INV_MERIDIONAL_DIST   = -17
!! non-convergent inverse meridional dist
!!
!> FPJ4_NON_CONVERGENT_INV_PHI2              = -18
!! non-convergent inverse phi2
!!
!> FPJ4_INVALID_ANGLE                        = -19
!! acos/asin: |arg| >1.+1e-14
!!
!> FPJ4_TOLERANCE_CONDITION_ERR              = -20
!! tolerance condition error
!!
!> FPJ4_INVALID_CONIC_LAT1_LAT2              = -21
!! conic lat_1 = -lat_2
!!
!> FPJ4_LAT1_OUT_OF_BOUNDS                   = -22
!! lat_1 >= 90
!!
!> FPJ4_INVALID_LAT1                         = -23
!! lat_1 = 0
!!
!> FPJ4_LAT_TS_OUT_OF_BOUNDS                 = -24
!! lat_ts >= 90
!!
!> FPJ4_INVALID_CTRL_POINTS_DIST             = -25
!! no distance between control points
!!
!> FPJ4_PROJ_MISS_SELECT_ROTATED             = -26
!! projection not selected to be rotated
!!
!> FPJ4_INVALID_W_M                          = -27
!! W <= 0 or M <= 0
!!
!> FPJ4_LSAT_OUT_OF_RANGE                    = -28
!! lsat not in 1-5 range
!!
!> FPJ4_PATH_OUT_OF_RANGE                    = -29
!! path not in range
!!
!> FPJ4_INVALID_H                            = -30
!! h <= 0
!!
!> FPJ4_INVALID_K                            = -31
!! k <= 0
!!
!> FPJ4_INVALID_LAT0_ALPHA                   = -32
!! lat_0 = 0 or 90 or alpha = 90
!!
!> FPJ4_INVALID_LAT1_LAT2                    = -33
!! lat_1=lat_2 or lat_1=0 or lat_2=90
!!
!> FPJ4_ELLIPTICAL_REQUIRED                  = -34
!! elliptical usage required
!!
!> FPJ4_INVALID_UTM_ZONE                     = -35
!! invalid UTM zone number
!!
!> FPJ4_TCHEBY_ARGS_OUT_OF_RANGE             = -36
!! arg(s) out of range for Tcheby eval
!!
!> FPJ4_PROJ_NOT_FOUND_ROTATED               = -37
!! failed to find projection to be rotated
!!
!> FPJ4_DATUM_SHIFT_LOAD_ERR                 = -38
!! failed to load datum shift file
!!
!> FPJ4_MN_DEFINITION_ERR                    = -39
!! both n & m must be spec'd and > 0
!!
!> FPJ4_N_DEFINITION_ERR                     = -40
!! n <= 0, n > 1 or not specified
!!
!> FPJ4_MISS_LAT1_LAT2                       = -41
!! lat_1 or lat_2 not specified
!!
!> FPJ4_INCONCISTENT_LAT1_LAT2               = -42
!! |lat_1| == |lat_2|
!!
!> FPJ4_LAT0_DEFINED_FROM_MEAN_LAT           = -43
!! lat_0 is pi/2 from mean lat
!!
!> FPJ4_PROJ_DEFINITION_PARSE_ERR            = -44
!! unparseable coordinate system definition
!!
!> FPJ4_MISS_GEOCENTRIC_PARAM                = -45
!! geocentric transformation missing z or ellps
!!
!> FPJ4_UNKNOWN_PRIME_MERIDIAN_CONV_ID       = -46
!! unknown prime meridian conversion id
!!
!> FPJ4_AXIS_ORIENTATION_ERR                 = -47
!! illegal axis orientation combination
!!
!> FPJ4_POINT_NOT_FOUND_DATUM_SHIFT_GRIDS    = -48
!! point not within available datum shift grids
!!
!> FPJ4_INVALID_SWEEP_AXIS                   = -49
!! invalid sweep axis, choose x or y
!!
!> FPJ4_MALFORMED_PIPELINE                   = -50
!! malformed pipeline
!!
!> FPJ4_INVALID_UNIT_CONV_FACTOR             = -51
!! unit conversion factor must be > 0
!!
!> FPJ4_INVALID_SCALE                        = -52
!! invalid scale
!!
!> FPJ4_NON_CONVERGENT_COMPUTATION           = -53
!! non-convergent computation
!!
!> FPJ4_MISS_ARGS                            = -54
!! missing required arguments
!!
!> FPJ4_INVALID_LAT0                         = -55
!! lat_0 = 0
!!
!> FPJ4_ELLIPSOIDAL_USAGE_UNSUPPORTED        = -56
!! ellipsoidal usage unsupported
!!
!> FPJ4_INVALID_INIT_PARAMETER               = -57
!! only one +init allowed for non-pipeline operations
!!
!> FPJ4_INVALID_ARG                          = -58
!! argument not numerical or out of range
!!
!> FPJ4_INCONSISTENT_UNIT_TYPES              = -59
!! inconsistent unit type between input and output
