

#include <projects.h>

/*
 * error string
 */
int bind_pj_strerrno(int err, char *err_msg)
{
    char* msg = pj_strerrno(err);
    if (msg != NULL)
        strcpy(err_msg, msg);
    return pj_ctx_get_errno(pj_get_default_ctx());
}

/*
 * initialise projection structure
 */
int bind_pj_init_plus(projPJ *prjdefn, char *args)
{
    *prjdefn = pj_init_plus(args);
	return pj_ctx_get_errno(pj_get_default_ctx());
}

/*
 * free projection structure
 */
int bind_pj_free(projPJ *prjdefn)
{
    pj_free(*prjdefn);
	return pj_ctx_get_errno(pj_get_default_ctx());
}

/*
 * transform projection
 */
int bind_pj_transform(projPJ *srcdefn, projPJ *dstdefn,
                         long point_count, int point_offset,
                         double *x, double *y, double *z)
{
    return  pj_transform(*srcdefn, *dstdefn,
                         point_count, point_offset, x, y, z);
}
