
module fproj4

    use iso_c_binding

    ! Chekout for errno source code in the pj_strerrno.c

    integer, parameter :: FPJ4_NOERR                                =   0 ! errors not found
    integer, parameter :: FPJ4_MISS_INIT_LIST_ARGS                  =  -1 ! no arguments in initialization list
    integer, parameter :: FPJ4_MISS_INIT_FILE_OPTS                  =  -2 ! no options found in 'init' file
    integer, parameter :: FPJ4_MISS_COLON_INIT_STR                  =  -3 ! no colon in init= string
    integer, parameter :: FPJ4_MISS_PROJ_NAME                       =  -4 ! projection not named
    integer, parameter :: FPJ4_UNKNOWN_PROJ_ID                      =  -5 ! unknown projection id
    integer, parameter :: FPJ4_INVALID_EFFECT_ECCENTRICITY          =  -6 ! effective eccentricity = 1
    integer, parameter :: FPJ4_UNKNOWN_UNIT_CONV_ID                 =  -7 ! unknown unit conversion id
    integer, parameter :: FPJ4_INVALID_BOOL_PARAM                   =  -8 ! invalid boolean param argument
    integer, parameter :: FPJ4_UNKNOWN_ELLIPTICAL_PARAM             =  -9 ! unknown elliptical parameter name
    integer, parameter :: FPJ4_INVALID_FLATTENING                   = -10 ! reciprocal flattening (1/f) = 0
    integer, parameter :: FPJ4_INVALID_RADIUS_REF_LAT               = -11 ! |radius reference latitude| > 90
    integer, parameter :: FPJ4_INVALID_ECCENTRICITY                 = -12 ! squared eccentricity < 0
    integer, parameter :: FPJ4_MISS_EARTH_RADIUS                    = -13 ! major axis or radius = 0 or not given
    integer, parameter :: FPJ4_LAT_LON_OUT_OF_BOUNDS                = -14 ! latitude or longitude exceeded limits
    integer, parameter :: FPJ4_INVALID_X_Y                          = -15 ! invalid x or y
    integer, parameter :: FPJ4_DMS_FORMAT_ERR                       = -16 ! improperly formed DMS value
    integer, parameter :: FPJ4_NON_CONVERGENT_INV_MERIDIONAL_DIST   = -17 ! non-convergent inverse meridional dist
    integer, parameter :: FPJ4_NON_CONVERGENT_INV_PHI2              = -18 ! non-convergent inverse phi2
    integer, parameter :: FPJ4_INVALID_ANGLE                        = -19 ! acos/asin: |arg| >1.+1e-14
    integer, parameter :: FPJ4_TOLERANCE_CONDITION_ERR              = -20 ! tolerance condition error
    integer, parameter :: FPJ4_INVALID_CONIC_LAT1_LAT2              = -21 ! conic lat_1 = -lat_2
    integer, parameter :: FPJ4_LAT1_OUT_OF_BOUNDS                   = -22 ! lat_1 >= 90
    integer, parameter :: FPJ4_INVALID_LAT1                         = -23 ! lat_1 = 0
    integer, parameter :: FPJ4_LAT_TS_OUT_OF_BOUNDS                 = -24 ! lat_ts >= 90
    integer, parameter :: FPJ4_INVALID_CTRL_POINTS_DIST             = -25 ! no distance between control points
    integer, parameter :: FPJ4_PROJ_MISS_SELECT_ROTATED             = -26 ! projection not selected to be rotated
    integer, parameter :: FPJ4_INVALID_W_M                          = -27 ! W <= 0 or M <= 0
    integer, parameter :: FPJ4_LSAT_OUT_OF_RANGE                    = -28 ! lsat not in 1-5 range
    integer, parameter :: FPJ4_PATH_OUT_OF_RANGE                    = -29 ! path not in range
    integer, parameter :: FPJ4_INVALID_H                            = -30 ! h <= 0
    integer, parameter :: FPJ4_INVALID_K                            = -31 ! k <= 0
    integer, parameter :: FPJ4_INVALID_LAT0_ALPHA                   = -32 ! lat_0 = 0 or 90 or alpha = 90
    integer, parameter :: FPJ4_INVALID_LAT1_LAT2                    = -33 ! lat_1=lat_2 or lat_1=0 or lat_2=90
    integer, parameter :: FPJ4_ELLIPTICAL_REQUIRED                  = -34 ! elliptical usage required
    integer, parameter :: FPJ4_INVALID_UTM_ZONE                     = -35 ! invalid UTM zone number
    integer, parameter :: FPJ4_TCHEBY_ARGS_OUT_OF_RANGE             = -36 ! arg(s) out of range for Tcheby eval
    integer, parameter :: FPJ4_PROJ_NOT_FOUND_ROTATED               = -37 ! failed to find projection to be rotated
    integer, parameter :: FPJ4_DATUM_SHIFT_LOAD_ERR                 = -38 ! failed to load datum shift file
    integer, parameter :: FPJ4_MN_DEFINITION_ERR                    = -39 ! both n & m must be spec'd and > 0
    integer, parameter :: FPJ4_N_DEFINITION_ERR                     = -40 ! n <= 0, n > 1 or not specified
    integer, parameter :: FPJ4_MISS_LAT1_LAT2                       = -41 ! lat_1 or lat_2 not specified
    integer, parameter :: FPJ4_INCONCISTENT_LAT1_LAT2               = -42 ! |lat_1| == |lat_2|
    integer, parameter :: FPJ4_LAT0_DEFINED_FROM_MEAN_LAT           = -43 ! lat_0 is pi/2 from mean lat
    integer, parameter :: FPJ4_PROJ_DEFINITION_PARSE_ERR            = -44 ! unparseable coordinate system definition
    integer, parameter :: FPJ4_MISS_GEOCENTRIC_PARAM                = -45 ! geocentric transformation missing z or ellps
    integer, parameter :: FPJ4_UNKNOWN_PRIME_MERIDIAN_CONV_ID       = -46 ! unknown prime meridian conversion id
    integer, parameter :: FPJ4_AXIS_ORIENTATION_ERR                 = -47 ! illegal axis orientation combination
    integer, parameter :: FPJ4_POINT_NOT_FOUND_DATUM_SHIFT_GRIDS    = -48 ! point not within available datum shift grids
    integer, parameter :: FPJ4_INVALID_SWEEP_AXIS                   = -49 ! invalid sweep axis, choose x or y
    integer, parameter :: FPJ4_MALFORMED_PIPELINE                   = -50 ! malformed pipeline
    integer, parameter :: FPJ4_INVALID_UNIT_CONV_FACTOR             = -51 ! unit conversion factor must be > 0
    integer, parameter :: FPJ4_INVALID_SCALE                        = -52 ! invalid scale
    integer, parameter :: FPJ4_NON_CONVERGENT_COMPUTATION           = -53 ! non-convergent computation
    integer, parameter :: FPJ4_MISS_ARGS                            = -54 ! missing required arguments
    integer, parameter :: FPJ4_INVALID_LAT0                         = -55 ! lat_0 = 0
    integer, parameter :: FPJ4_ELLIPSOIDAL_USAGE_UNSUPPORTED        = -56 ! ellipsoidal usage unsupported
    integer, parameter :: FPJ4_INVALID_INIT_PARAMETER               = -57 ! only one +init allowed for non-pipeline operations
    integer, parameter :: FPJ4_INVALID_ARG                          = -58 ! argument not numerical or out of range
    integer, parameter :: FPJ4_INCONSISTENT_UNIT_TYPES              = -59 ! inconsistent unit type between input and output

    type fproj4_prj
        private
        type(c_ptr) :: prj
    end type fproj4_prj

    real(kind=8), private, parameter :: PI = 4._8 * datan(1._8) 
    real(kind=8), parameter :: RAD2DEG = 180._8 / PI
    real(kind=8), parameter :: DEG2RAD = PI / 180._8
    character(len=*), parameter :: LATLONG_PRJ_STR = "+proj=latlong "//&
                                                     "+ellps=WGS84 "//&
                                                     "+datum=WGS84"

    interface fproj4_transform
        !> @param[in] srcdefn Source (input) coordinate system.
        !!
        !> @param[in] dstdefn Destination (output) coordinate system.     
        !!
        !> @param[inout] x, y, z The array/element of X, Y and Z coordinate values
        !!                        passed as input, and modified in place for output.
        !!                        The Z may be optional.
        !!
        !> @param[in] point_offset The step size from value to value (measured in doubles)
        !!                          within the x/y/z arrays - normally 1 for a
        !!                          packed array. May be used to operate on xyz interleaved
        !!                          point arrays. It is an optional argument.
        !!
        !> @return stat The return is zero on success, or a PROJ.4 error code.
        module procedure fproj4_transform_pt,&
                         fproj4_transform_array,&
                         fproj4_transform_array_2d
    end interface

    interface fproj4_transfer
        !> @param[in] srcdefn Source (input) coordinate system.
        !!                      
        !> @param[in] dstdefn Destination (output) coordinate system.     
        !!                      
        !> @param[in] x, y The array/element of X and Y coordinate values
        !!                 passed as input, in the source coordinate system.
        !!                      
        !> @param[out] x_t, y_t The array/element of X and Y coordinate values
        !!                      where the output will be stored, in the
        !!                      destination coordinate system.
        !!                      
        !> @return stat The return is zero on success, or a PROJ.4 error code.
        module procedure fproj4_transfer_pt,&
                         fproj4_transfer_array,&
                         fproj4_transfer_array_2d
    end interface

    interface fproj4_fwd
        !> @param[in] prjdefn Destination (output)  coordinate system.
        !!                      
        !> @param[in] x, y The array/element of X and Y coordinate values
        !!                 passed as input, in the Geographic (long,lat)
        !!                 coordinate system.
        !!                      
        !> @param[out] x_t, y_t The array/element of X and Y coordinate values
        !!                      where the output will be stored, in the
        !!                      destination coordinate system.
        !!                      
        !> @return stat The return is zero on success, or a PROJ.4 error code.
        module procedure fproj4_fwd_pt,&
                         fproj4_fwd_array,&
                         fproj4_fwd_array_2d
    end interface

    interface fproj4_inv
        !> @param[in] prjdefn Source (input)   coordinate system.
        !!                      
        !> @param[in] x, y The array/element of X and Y coordinate values
        !!                 passed as input, in the source coordinate system.
        !!                      
        !> @param[out] x_t, y_t The array/element of X and Y coordinate values
        !!                      where the output will be stored, in the Geographic
        !!                      (long,lat) coordinate system.
        !!                      
        !> @return stat The return is zero on success, or a PROJ.4 error code.
        module procedure fproj4_inv_pt,&
                         fproj4_inv_array,&
                         fproj4_inv_array_2d
    end interface

    interface
        function pj_init_plus_f(prjdefn, prjstr)&
                                bind(c, name='bind_pj_init_plus')&
                                result(stat)
            use iso_c_binding
            type(c_ptr), intent(out) :: prjdefn
            character(C_CHAR)        :: prjstr(*)
            integer(C_INT)           :: stat
        end function pj_init_plus_f
    end interface

    interface
        function pj_free_f(prjdefn)&
                                bind(c, name='bind_pj_free')&
                                result(stat)
            use iso_c_binding
            type(c_ptr), intent(in) :: prjdefn
            integer(C_INT)          :: stat
        end function pj_free_f
    end interface

    interface
        function pj_strerrno_f(err, err_msg)&
                                bind(c, name='bind_pj_strerrno')&
                                result(stat)
            use iso_c_binding
            integer(C_INT), value, intent(in) :: err
            character(C_CHAR), intent(out)    :: err_msg(*)
            integer(C_INT)                    :: stat
        end function pj_strerrno_f
    end interface

    interface
        function pj_transform_f(srcdefn, dstdefn, point_count, point_offset,&
                                x, y, z) bind(c, name='bind_pj_transform')&
                                result(stat)
            use iso_c_binding
            type(c_ptr), intent(in)            :: srcdefn, dstdefn
            integer(C_LONG), value, intent(in) :: point_count
            integer(C_INT), value, intent(in)  :: point_offset
            real(C_DOUBLE), intent(inout)      :: x, y, z
            integer(C_INT)                     :: stat
        end function pj_transform_f
    end interface


contains

    function fproj4_strerrno(prj_errno)
        implicit none
        character(len=80)   :: fproj4_strerrno
        integer, intent(in) :: prj_errno

        integer(C_INT)      :: stat

        fproj4_strerrno = ""
        stat = pj_strerrno_f(prj_errno, fproj4_strerrno)
    end function fproj4_strerrno

    function fproj4_init(prj, prjstr)
        implicit none
        integer(C_INT)                :: fproj4_init
        type(fproj4_prj), intent(out) :: prj
        character(len=*), intent(in)  :: prjstr

        fproj4_init = pj_init_plus_f(prj%prj, trim(prjstr)//C_NULL_CHAR)
    end function fproj4_init

    function fproj4_free(prj)
        implicit none
        integer(C_INT)                        :: fproj4_free
        type(fproj4_prj), intent(in) :: prj

        fproj4_free =  pj_free_f(prj%prj)
    end function fproj4_free

    function fproj4_transform_pt(srcdefn, dstdefn, x, y, z, point_offset) result(stat)
        implicit none
        integer(C_INT)                       :: stat
        type(fproj4_prj), intent(in)   :: srcdefn
        type(fproj4_prj), intent(in)   :: dstdefn
        real(C_DOUBLE), target, intent(in)   :: x, y
        real(C_DOUBLE), optional, intent(in) :: z
        integer(C_INT), optional, intent(in) :: point_offset

        real(C_DOUBLE), target  :: z_
        real(C_DOUBLE), pointer :: x_ptr, y_ptr, z_ptr
        integer(C_LONG)         :: point_count
        integer(C_INT)          :: point_offset_
 
        if (present(z)) then
            z_ = z
        else
            z_ = 0
        end if
        point_offset_ = merge(point_offset, 1, present(point_offset))
        x_ptr => x; y_ptr => y; z_ptr => z_
        point_count = 1
        stat = pj_transform_f(srcdefn%prj, dstdefn%prj,&
                              point_count, point_offset_,&
                              x_ptr, y_ptr, z_ptr)
    end function fproj4_transform_pt

    function fproj4_transform_array(srcdefn, dstdefn, x, y, z, point_offset) result(stat)
        implicit none
        integer(C_INT)                       :: stat
        type(fproj4_prj), intent(in)   :: srcdefn
        type(fproj4_prj), intent(in)   :: dstdefn
        real(C_DOUBLE), target, intent(in)   :: x(:), y(:)
        real(C_DOUBLE), optional, intent(in) :: z(:)
        integer(C_INT), optional, intent(in) :: point_offset

        real(C_DOUBLE), target  :: z_(size(x))
        real(C_DOUBLE), pointer :: x_ptr, y_ptr, z_ptr
        integer(C_LONG)         :: point_count
        integer(C_INT)          :: point_offset_
 
        if (present(z)) then
            z_ = z
        else
            z_ = 0
        end if
        point_offset_ = merge(point_offset, 1, present(point_offset))
        x_ptr => x(1); y_ptr => y(1); z_ptr => z_(1)
        point_count = size(x)
        stat = pj_transform_f(srcdefn%prj, dstdefn%prj,&
                              point_count, point_offset_,&
                              x_ptr, y_ptr, z_ptr)
    end function fproj4_transform_array

    function fproj4_transform_array_2d(srcdefn, dstdefn, x, y, z, point_offset) result(stat)
        implicit none
        integer(C_INT)                       :: stat
        type(fproj4_prj), intent(in)   :: srcdefn
        type(fproj4_prj), intent(in)   :: dstdefn
        real(C_DOUBLE), target, intent(in)   :: x(:,:), y(:,:)
        real(C_DOUBLE), optional, intent(in) :: z(:,:)
        integer(C_INT), optional, intent(in) :: point_offset

        real(C_DOUBLE), target  :: z_(size(x,1), size(x,2))
        real(C_DOUBLE), pointer :: x_ptr, y_ptr, z_ptr
        integer(C_LONG)         :: point_count
        integer(C_INT)          :: point_offset_
 
        if (present(z)) then
            z_ = z
        else
            z_ = 0
        end if
        point_offset_ = merge(point_offset, 1, present(point_offset))
        x_ptr => x(1,1); y_ptr => y(1,1); z_ptr => z_(1,1)
        point_count = size(x)
        stat = pj_transform_f(srcdefn%prj, dstdefn%prj,&
                              point_count, point_offset_,&
                              x_ptr, y_ptr, z_ptr)
    end function fproj4_transform_array_2d

    function fproj4_transfer_pt(srcdefn, dstdefn, x, y, x_t, y_t) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: x, y
        real(C_DOUBLE), target, intent(out) :: x_t, y_t
        type(fproj4_prj), intent(in)  :: srcdefn
        type(fproj4_prj), intent(in)  :: dstdefn
 
        x_t = x 
        y_t = y 
        stat = fproj4_transform(srcdefn, dstdefn, x_t, y_t)
    end function fproj4_transfer_pt

    function fproj4_transfer_array(srcdefn, dstdefn, x, y, x_t, y_t) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: x(:), y(:)
        real(C_DOUBLE), target, intent(out) :: x_t(:), y_t(:)
        type(fproj4_prj), intent(in)  :: srcdefn
        type(fproj4_prj), intent(in)  :: dstdefn

        x_t = x 
        y_t = y 
        stat = fproj4_transform(srcdefn, dstdefn, x_t, y_t)
    end function fproj4_transfer_array

    function fproj4_transfer_array_2d(srcdefn, dstdefn, x, y, x_t, y_t) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: x(:,:), y(:,:)
        real(C_DOUBLE), target, intent(out) :: x_t(:,:), y_t(:,:)
        type(fproj4_prj), intent(in)  :: srcdefn
        type(fproj4_prj), intent(in)  :: dstdefn

        x_t = x 
        y_t = y 
        stat = fproj4_transform(srcdefn, dstdefn, x_t, y_t)
    end function fproj4_transfer_array_2d

    function fproj4_fwd_pt(dstdefn, lam, phi, x, y) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: lam, phi
        real(C_DOUBLE), target, intent(out) :: x, y
        type(fproj4_prj), intent(in)  :: dstdefn

        type(fproj4_prj) :: srcdefn

        stat = fproj4_init(srcdefn, LATLONG_PRJ_STR)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_transfer(srcdefn, dstdefn, lam*DEG2RAD, phi*DEG2RAD, x, y)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_free(srcdefn)
    end function fproj4_fwd_pt

    function fproj4_fwd_array(dstdefn, lam, phi, x, y) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: lam(:), phi(:)
        real(C_DOUBLE), target, intent(out) :: x(:), y(:)
        type(fproj4_prj), intent(in)  :: dstdefn

        type(fproj4_prj) :: srcdefn

        stat = fproj4_init(srcdefn, LATLONG_PRJ_STR)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_transfer(srcdefn, dstdefn, lam*DEG2RAD, phi*DEG2RAD, x, y)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_free(srcdefn)
    end function fproj4_fwd_array

    function fproj4_fwd_array_2d(dstdefn, lam, phi, x, y) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: lam(:,:), phi(:,:)
        real(C_DOUBLE), target, intent(out) :: x(:,:), y(:,:)
        type(fproj4_prj), intent(in)  :: dstdefn

        type(fproj4_prj) :: srcdefn

        stat = fproj4_init(srcdefn, LATLONG_PRJ_STR)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_transfer(srcdefn, dstdefn, lam*DEG2RAD, phi*DEG2RAD, x, y)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_free(srcdefn)
    end function fproj4_fwd_array_2d
 
    function fproj4_inv_pt(srcdefn, x, y, lam, phi) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: x, y
        real(C_DOUBLE), target, intent(out) :: lam, phi
        type(fproj4_prj), intent(in)  :: srcdefn

        type(fproj4_prj) :: dstdefn

        stat = fproj4_init(dstdefn, LATLONG_PRJ_STR)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_transfer(srcdefn,dstdefn, x, y, lam, phi)
        if (stat .ne. FPJ4_NOERR) return
        lam = lam * RAD2DEG
        phi = phi * RAD2DEG
        stat = fproj4_free(dstdefn)
    end function fproj4_inv_pt   

    function fproj4_inv_array(srcdefn, x, y, lam, phi) result(stat)
        implicit none
        integer(C_INT)                      :: stat
        real(C_DOUBLE), intent(in)          :: x(:), y(:)
        real(C_DOUBLE), target, intent(out) :: lam(:), phi(:)
        type(fproj4_prj), intent(in)  :: srcdefn

        type(fproj4_prj) :: dstdefn

        stat = fproj4_init(dstdefn, LATLONG_PRJ_STR)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_transfer(srcdefn,dstdefn, x, y, lam, phi)
        if (stat .ne. FPJ4_NOERR) return
        lam = lam * RAD2DEG
        phi = phi * RAD2DEG
        stat = fproj4_free(dstdefn)
    end function fproj4_inv_array   

    function fproj4_inv_array_2d(srcdefn, x, y, lam, phi) result(stat)
        implicit none
        integer(C_INT)                        :: stat
        real(C_DOUBLE), intent(in)            :: x(:,:), y(:,:)
        real(C_DOUBLE), target, intent(out)   :: lam(:,:), phi(:,:)
        type(fproj4_prj), intent(in)    :: srcdefn

        type(fproj4_prj)                :: dstdefn

        stat = fproj4_init(dstdefn, LATLONG_PRJ_STR)
        if (stat .ne. FPJ4_NOERR) return
        stat = fproj4_transfer(srcdefn,dstdefn, x, y, lam, phi)
        if (stat .ne. FPJ4_NOERR) return
        lam = lam * RAD2DEG
        phi = phi * RAD2DEG
        stat = fproj4_free(dstdefn)
    end function fproj4_inv_array_2d

end module fproj4
